#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <sqlite3.h>
#include "database.h"

int main(int argc, char* argv[]){
	if(!file_exists(DATABASE)){
		sqlite3* database; 
		sqlite3_open(DATABASE, &database);
		if(create_threads_table(database)){
			fprintf(stderr, "Błąd utworzenia tabeli tematów.");
			if(std::remove(DATABASE)){
				fprintf(stdout, "Usunięto plik bazy danych.");
				return 0;
			}else{
				fprintf(stderr, "Nie udało się usunąć pliku bazy danych.");
				return 1;
			}	
		}else{
			fprintf(stdout, "Tabela tematów utworzona.");
		}
		if(create_images_table(database)){
			fprintf(stderr, "Błąd utworzenia tabeli obrazków.");
			if(std::remove(DATABASE)){
				fprintf(stdout, "Usunięto plik bazy danych.");
				return 0;
			}else{
				fprintf(stderr, "Nie udało się usunąć pliku bazy danych.");
				return 1;
			}	
		}else{
			fprintf(stdout, "Tabela obrazków utworzona.");
		}
		sqlite3_close(database);		
	}
	return -1;
}

