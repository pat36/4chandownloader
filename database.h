#ifndef DATABASE_H
#define DATABASE_H

#define DATABASE "database.db"

//Jeśli funkcja zwróci 1 to plik istnieje, jeśli 0 to nie istnieje.
int file_exists(char* filename){
	std::ifstream infile(filename);	
	return infile.good() ? 1 : 0;
}

int create_threads_table(sqlite3* database){
	char* sql = "CREATE TABLE Threads(" \
		     "id INT PRIMARY KEY NOT NULL," \
		     "com TEXT NOT NULL," \
		     "imagelimit INT NOT NULL," \
		     "images INT NOT NULL" \
		     ");";
	return sqlite3_exec(database, sql, 0, 0, 0) ? 1 : 0;
}

int create_images_table(sqlite3* database){
	char* sql = "CREATE TABLE Images(" \
		    "id TEXT PRIMARY KEY NOT NULL," \
		    "thread INT NOT NULL," \
		    "FOREIGN KEY(thread) REFERENCES Threads(id)" \
		    ");";
	return sqlite3_exec(database, sql, 0, 0, 0) ? 1 : 0;

}
#endif
